var axios = require('axios');
var JSZip = require('jszip');
var Docxtemplater = require('docxtemplater');
var expressions = require('angular-expressions');
var fs = require('fs');
var path = require('path');


//Funcion que hace la peticion al servidor para recibir la informacion
function getExp(expediente) {
    var url = `http://minotaria.net/base/_pruebas/db_export.php?ver=todo&exp=${expediente}`

    return axios.get(url)
        .then(res => {
            return res.data[0]
        })
        .catch(err => console.log(`${err}`))
}

getExp('17/7/1')
    .then(data => {
        //conversionDoc(data)
				fs.writeFileSync(path.resolve(__dirname, '../files-test/datos_exp.json'), JSON.stringify(data));
				console.log(data);
    })
    .catch(err => console.log(`${err}`))



/*expressions.filters.lower = function (input) {
    if (!input) return input;
    return input.toLowerCase();
}

var angularParser = function (tag) {
    return {
        get: tag === '.' ? function (s) {
            return s;
        } : expressions.compile(tag)
    };
}

//Funcion que inserta los datos del objeto en el documento .docx
function conversionDoc(data) {
    //Load the docx file as a binary
    var content = fs
        .readFileSync(path.resolve(__dirname, 'input.docx'), 'binary');

    var zip = new JSZip(content);

    var doc = new Docxtemplater();
    doc.loadZip(zip);

    doc.setOptions({
        parser: angularParser
    });

    //set the templateVariables
    doc.setData(data);

    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render()
    } catch (error) {
        var e = {
            message: error.message,
            name: error.name,
            stack: error.stack,
            properties: error.properties,
        }
        console.log(JSON.stringify({
            error: e
        }));
        // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
        throw error;
    }

    var buf = doc.getZip()
        .generate({
            type: 'nodebuffer'
        });

    // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
    fs.writeFileSync(path.resolve(__dirname, 'output.docx'), buf);
    console.log('Finalizado!!');
}*/