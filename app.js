var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var docxtemplater = require("docxtemplater");
var path = require("path");
var dotize = require("dotize");
var upload = require("./models/upload");
var model = require("./models/data-to-model");
var labels = require("./models/replace-labels");

var port = process.env.PORT || 2000;

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "/views"));
app.use(
	bodyParser.urlencoded({
		extended: true
	})
);
app.use(bodyParser.json());

app.get("/", function(req, res) {
	res.render("upload");
});

app.post("/upload", function(req, res) {
	upload.searchDocs(req, res, function(err, docx, json) {
		if (err) {
			res.send(`Archivo docx o json con extension invalida ${err}`);
		} else if (!docx) {
			var jsonModel = model.dataToModel(json.path);
			jsonModel = dotize.convert(jsonModel);
			res.send(jsonModel);
		} else {
			var jsonModel = model.dataToModel(json.path);

			labels.replace(docx, jsonModel, function(newDocx) {
				res.sendFile(path.join(__dirname + newDocx));
			});
		}
	});
});

app.listen(port);
