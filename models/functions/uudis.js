var uuidv1 = require('uuid/v1');
var uuidv5 = require('uuid/v5');

var MY_NAMESPACE = uuidv1();
/*var name = {
	'cer': 'Certificacion.docx',
	'con': 'Cosnsulta.docx',
	'ins': 'Inscripcion.docx',
	'zon': 'Zonificacion.docx'
}*/

exports.createUUID = function (name) {
	return uuidv5(name, MY_NAMESPACE);
}
