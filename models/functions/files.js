exports.validateDocx = function(files) {
	if (!files.docx) {
		return false;
	}
	var fileDocx = files.docx[0].originalFilename;

	var expReg = /^.*\.(docx)$/i;
	var fileTrue = expReg.test(fileDocx);

	if (!fileTrue) {
		return false;
	}
	return true;
};

exports.validateJson = function(files) {
	var fileJson = files.json[0].originalFilename;

	var expReg = /^.*\.(json)$/i;
	var fileTrue = expReg.test(fileJson);

	if (!fileTrue) {
		return false;
	}
	return true;
};
