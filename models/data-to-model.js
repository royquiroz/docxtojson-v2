var fs = require("fs");
var path = require("path");

var dotty = require("dotty");
var dotize = require("dotize");

var modelJson = require("../models/template_json");

var dotNotaria = dotize.convert(modelJson.notaria);
var dotExpediente = dotize.convert(modelJson.expediente);
var dotComparecientes = dotize.convert(modelJson.comparecientes);
var dotInmuebles = dotize.convert(modelJson.inmuebles);
var dotAutos = dotize.convert(modelJson.autos);
var dotObs = dotize.convert(modelJson.obs);
var dotOperaciones = dotize.convert(modelJson.operaciones);

exports.dataToModel = function(data) {
	var data = fs.readFileSync(data, "utf-8");
	data = JSON.parse(data);
	data = data[0];
	//console.log(data);

	for (var key in dotNotaria) {
		var propModel = key.replace("[0]", ".0");
		var valModel = dotNotaria[key];

		var result = dotty.get(data, valModel);

		dotty.put(modelJson.notaria, propModel, result);
	}

	for (var key in dotExpediente) {
		var propModel = key.replace("[0]", ".0");
		var valModel = dotExpediente[key];

		var result = dotty.get(data, valModel);

		dotty.put(modelJson.expediente, propModel, result);
	}

	for (var key in dotComparecientes) {
		if (data.comparecientes[0].comparecientes.length > 0) {
			for (var i = 0; i < data.comparecientes[0].comparecientes.length; i++) {
				propModel = key.replace("[0]", "comparecientes." + i);

				valModel = dotComparecientes[key].replace(
					"comparecientes.0.comparecientes.0",
					"comparecientes.0.comparecientes." + i
				);

				var result = dotty.get(data, valModel);

				dotty.put(modelJson, propModel, result);
			}
		}
	}

	for (var key in dotInmuebles) {
		if (data.inmuebles[0].inmuebles.length > 0) {
			for (var i = 0; i < data.inmuebles[0].inmuebles.length; i++) {
				propModel = key.replace("[0]", "inmuebles." + i);

				valModel = dotInmuebles[key].replace(
					"inmuebles.0.inmuebles.0",
					"inmuebles.0.inmuebles." + i
				);

				var result = dotty.get(data, valModel);

				dotty.put(modelJson, propModel, result);
			}
		}
	}

	for (var key in dotAutos) {
		if (data.autos[0].datos_auto_expedientes.length > 0) {
			for (var i = 0; i < data.autos[0].datos_auto_expedientes.length; i++) {
				propModel = key.replace("[0]", "autos." + i);

				valModel = dotAutos[key].replace(
					"autos.0.datos_auto_expedientes.0",
					"autos.0.datos_auto_expedientes." + i
				);

				var result = dotty.get(data, valModel);

				dotty.put(modelJson, propModel, result);
			}
		}
	}

	for (var key in dotOperaciones) {
		if (data.comparecientes[3].operaciones.length > 0) {
			for (var i = 0; i < data.comparecientes[3].operaciones.length; i++) {
				var propModel = key.replace("[0]", "operaciones." + i);
				var valModel = dotOperaciones[key].replace(
					"comparecientes.3.operaciones.0",
					"comparecientes.3.operaciones." + i
				);

				var result = dotty.get(data, valModel);

				dotty.put(modelJson, propModel, result);
			}
		}
	}

	for (var key in dotObs) {
		if (data.obs.length > 0) {
			for (var i = 0; i < data.obs.length; i++) {
				var propModel = key.replace("[0]", "obs." + i);
				var valModel = dotObs[key].replace("obs.0", "obs." + i);

				var result = dotty.get(data, valModel);
				dotty.put(modelJson, propModel, result);
			}
		}
	}

	var nombres_todos = dotty.get(data, "comparecientes.1.nombres_todos");
	dotty.put(modelJson, "nombres_todos", nombres_todos);

	var generales_todos = dotty.get(data, "comparecientes.2.generales_todos");
	dotty.put(modelJson, "generales_todos", generales_todos);

	//console.log(modelJson);

	//fs.writeFileSync(path.resolve(__dirname, '../files-test/diccionario2.txt'), JSON.stringify(modelJson));
	return modelJson;
};
//console.log(modelJson);

//fs.writeFileSync('prueba.json', JSON.stringify(modelJson))

/*console.log(dotty.get(data, "expediente.0.expedientes.notarias.personas.nombre"));

dotty.put(modelJson, "notaria.notario.nombre", "Erick");
dotty.put(modelJson, "notaria.notario.apellido_paterno", "Espinosa");
dotty.put(modelJson, "notaria.notario.apellido_materno", "Rivera");
console.log(modelJson.notaria);*/
