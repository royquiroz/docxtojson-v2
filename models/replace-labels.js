var JSZip = require('jszip');
var Docxtemplater = require('docxtemplater');
var expressions = require('angular-expressions');
var fs = require('fs');
var path = require('path');
var uuid = require('./functions/uudis')

exports.replace = function (docx, data, callback) {
	expressions.filters.lower = function (input) {
		if (!input) return input;
		return input.toLowerCase();
	}

	var angularParser = function (tag) {
		return {
			get: tag === '.' ? function (s) {
				return s;
			} : expressions.compile(tag)
		};
	}


	var content = fs.readFileSync(docx.path, 'binary');

	var zip = new JSZip(content);

	var doc = new Docxtemplater();
	doc.loadZip(zip);

	doc.setOptions({
		parser: angularParser
	});

	//set the templateVariables
	doc.setData(data);

	try {
		// render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
		doc.render()
	} catch (error) {
		var e = {
			message: error.message,
			name: error.name,
			stack: error.stack,
			properties: error.properties,
		}
		console.log(JSON.stringify({
			error: e
		}));
		// The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
		throw error;
	}

	var buf = doc.getZip()
		.generate({
			type: 'nodebuffer'
		});

	var uuidNew = uuid.createUUID(docx.name);
	var routeNewDocx = '/files/' + uuidNew + '.docx'

	// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
	fs.writeFileSync(path.resolve(__dirname, '..' + routeNewDocx ), buf);

	callback(routeNewDocx)
}
