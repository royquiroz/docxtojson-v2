var multiparty = require("multiparty");
var http = require("http");
var fs = require("fs");

var validation = require("./functions/files");

exports.searchDocs = function(req, res, callback) {
	if (req.url === "/upload" && req.method === "POST") {
		var form = new multiparty.Form();

		form.parse(req, function(err, fields, files) {
			var docx = validation.validateDocx(files);
			var json = validation.validateJson(files);

			if (!docx && json) {
				var json = {
					name: files.json[0].originalFilename,
					path: files.json[0].path
				};
				callback(false, false, json);
			} else if (!docx) {
				callback(true, null, null);
			} else if (!json) {
				callback(true, null, null);
			} else {
				var docx = {
					name: files.docx[0].originalFilename,
					path: files.docx[0].path
				};
				var json = {
					name: files.json[0].originalFilename,
					path: files.json[0].path
				};

				callback(false, docx, json);
			}
		});
	}
};
